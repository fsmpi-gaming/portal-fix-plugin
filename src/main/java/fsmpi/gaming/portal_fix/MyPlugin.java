package fsmpi.gaming.portal_fix;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class MyPlugin extends JavaPlugin {
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new EntityPortalEventListener(getLogger()), this);
    }

    @Override
    public void onDisable() {
        
    }

    @Override
    public FileConfiguration getConfig() {
        if (super.getConfig().getCurrentPath().equals("")) {
            super.saveDefaultConfig();
        }
        return super.getConfig();
    }


}
