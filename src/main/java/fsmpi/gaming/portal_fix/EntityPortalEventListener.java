package fsmpi.gaming.portal_fix;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.World.Environment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEnterEvent;
import org.bukkit.event.entity.EntityPortalEvent;

public class EntityPortalEventListener implements Listener {
    private Logger logger;
    public EntityPortalEventListener(Logger logger) {
        this.logger = logger;
    }
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPortalEnterEvent(EntityPortalEnterEvent e) {
        if (e.getLocation().getWorld().getName().equals("farm") 
        && e.getLocation().distance(e.getLocation().getWorld().getSpawnLocation()) < 500) {
            e.getEntity().teleport(Bukkit.getWorld("main").getSpawnLocation());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityPortalEvent(EntityPortalEvent event) {
        if (event.getEntity().getWorld().getEnvironment() == Environment.THE_END && event.getTo().getWorld().getEnvironment() != Environment.THE_END) {
            event.setTo(event.getTo().getWorld().getSpawnLocation());
            logger.info("Send Entity " + event.getEntityType() + " to " + event.getTo().getWorld().getName() + " World Spawn");
        }
    }
}
